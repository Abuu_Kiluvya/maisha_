<?php defined( '_JEXEC' ) or die( 'Restricted access' );?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" 
   xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" >
<head>
<jdoc:include type="head" />
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/system.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/general.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/bootstrap.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/color.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/jquery.bxslider.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/responsive.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/custom.css" type="text/css" />

  <!--  Fonts -->
<link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/font/css/font-awesome.css" />
<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Raleway" />
 
<link rel="icon" href="images/favicon.ico" type="image/x-icon">
 
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

</head>
<body class="cp-theme-style-1">
 
<div id="wrapper">
 
<header id="header">
 
<section class="cp-head-topbar">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="left-box">
					<ul>
						<li><i class="fa fa-phone"></i> <a href="tel:+255655222332">+255 655 222 332</a></li>
						<li><i class="fa fa-envelope"></i>  <a href="mailto:info@maishasupermarket.co.tz"><span class="__cf_email__" ></span>info@maishasupermarket.co.tz</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>
 
 
<section class="cp-navigation-section">
	<div class="container"> <strong class="logo">
		<a href="<?= $this->baseurl; ?>" title="Maisha Supermarket">
			<img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/logo.png" alt="Maisha Supermarket Logo">
		</a>
		</strong>
		<div class="navigation-right">
			<div class="col-md-10">
				<nav class="navbar navbar-inverse">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
					</div>
					<div id="navbar" class="collapse navbar-collapse">
						<jdoc:include type="modules" name="topMenu" style="well" />

						<!--
						<ul id="nav" class="navbar-nav">
							<li class="active"><a href="index.php">Home</a></li>
							<li><a href="about.php">About Us</a>
							</li>
							<li><a href="offers.php">Offers</a>

							</li>
							<li><a href="contact.php">Contact Us</a>

							</li>
						</ul>
						-->
					</div>
				</nav>
			</div>

			<div class="col-md-2">
				 <ul class="footer-social">
					<li><a href="#"><i class="fa fa-facebook fa-lg"></i></a></li>
					<li><a href="#"><i class="fa fa-instagram fa-lg"></i></a></li>
				</ul> 
			</div>		 
		</div>
	</div>
</section>
 
</header> 
<jdoc:include type="modules" name="slider" style="well" />

<div class="container">
	<jdoc:include type="component" />
</div>
 
<footer id="footer">
	<section class="cp-footer-content">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-6">
				<div class="cp-box">
					<h3>Menus</h3>
					<jdoc:include type="modules" name="bottomMenu" style="well" />
					<!--
					<ul>
						<li>
						  <i class="fa fa-chevron-right"></i> <a href="index.php">Home</a> 
						</li>

						<li>
						  <i class="fa fa-chevron-right"></i> <a href="about.php">About Us</a> 
						</li>
						<li>
						  <i class="fa fa-chevron-right"></i> <a href="offers.php">Offers</a> 
						</li>
						<li>
						  <i class="fa fa-chevron-right"></i> <a href="contact.php">Contact Us</a> 
						</li>
					</ul>
					-->
				</div>
			</div>
			<div class="col-md-6 col-sm-6">
				<div class="cp-box">
					<h3>Be In Touch</h3>
					<address>
					<p>Maisha Supermarket City Mall, Bibi Titi Road <br>
					Dar Es Salaam, Tanzania.</p>
					<ul>
						<li><i class="fa fa-phone"></i> <a href="tel:+255655222332">+255 655 222 332</a></li>
						<li><i class="fa fa-envelope"></i>  <a href="mailto:info@maishasupermarket.co.tz"><span class="__cf_email__" ></span>info@maishasupermarket.co.tz</a></li>
					</ul>
						</address>
						<ul class="footer-social">
						<li><a href="#"><i class="fa fa-facebook fa-lg"></i></a></li>
						<li><a href="#"><i class="fa fa-instagram fa-lg"></i></a></li>
					</ul>

				</div>
			</div>

		</div>
		<div class="cp-copyright-section"> <strong class="copy">Maisha Supermarket &copy; <?= DATE("Y"); ?>, All Rights Reserved</a></strong> </div>
	</div>
	</section> 
</footer>
 
</div>
 
 
 
<script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/js/jquery-1.11.3.min.js"></script>
 
<script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/js/bootstrap.min.js"></script>
 
<script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/js/jquery.bxslider.min.js"></script>
 
<script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/js/custom.js"></script>
 
</body>
</html>
