<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Maisha_offers
 * @author     Fidelis <fidelis.charles23@gmail.com>
 * @copyright  Copyright (C) 2016. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::register('Maisha_offersFrontendHelper', JPATH_COMPONENT . '/helpers/maisha_offers.php');

// Execute the task.
$controller = JControllerLegacy::getInstance('Maisha_offers');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
