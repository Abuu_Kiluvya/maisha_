<?php 
// No direct access
defined('_JEXEC') or die;

$db = JFactory::getDBO();

$sql = "SELECT id,menutype,title,alias,path,link FROM #__menu WHERE published = 1 AND level = 1 AND menutype != 'menu' ORDER BY lft";
$db->setQuery($sql);
$res = $db->LoadObjectList();

/*
echo "<pre>";
print_r($res);
echo "</pre>";
*/
?>
<ul>
<?php
foreach( $res AS $thisMenuItem){
?>
	<li><i class="fa fa-chevron-right"></i><a href="<?= $thisMenuItem->path ?>" title="<?= $thisMenuItem->title; ?>"><?= $thisMenuItem->title ?></a></li>
<?php
}
?>
</ul>