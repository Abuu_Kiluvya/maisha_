<?php 
// No direct access
defined('_JEXEC') or die;

$tempArray = array();

for($i=1;$i<=5;$i++){
	if($params->get("image".$i)!=""){
		$temp = array( "src"=>$params->get("image".$i),
					   "link"=>$params->get("link".$i),
					   "heading"=>$params->get("heading".$i), 
					   "subheading"=>$params->get("subheading".$i), 
					   "calltoaction"=>$params->get("calltoaction".$i), 
					   );
		array_push($tempArray,$temp);
	}
}
/*
echo "<pre>";
print_r($tempArray);
echo "</pre>";
*/


?>
<div id="cp-banner">
	<ul id="cp-home-banner">

		<?php 
		foreach ($tempArray AS $thisSlider) {
		?>
			<li><img src="<?= $thisSlider["src"] ?>" alt="<?= $thisSlider["heading"]; ?>">
			<div class="caption"> <strong class="title"><?= $thisSlider["heading"]; ?></strong>
			<p><?= $thisSlider["subheading"]; ?></p>
			<div class="banner-btn-row"> <a href="<?= $thisSlider["link"]; ?>" class="btn-style-1"><?= $thisSlider["calltoaction"]; ?></a> </div>
			</div>
			</li>

		<?php
		}
		?>
	</ul>
</div>
 

